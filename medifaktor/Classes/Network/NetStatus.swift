//
//  NetStatus.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import Foundation
import Network

class NetStatus {
    static let shared = NetStatus()
    var monitor: NWPathMonitor?
    var isMonitoring = false
    
    var didStartMonitoringHandler: (() -> Void)?
    var didStopMonitoringHandler: (() -> Void)?
    var netStatusChangeHandler: (() -> Void)?
    
    private init() {
    }
    
    deinit {
        stopMonitoring()
    }
    
    var isConnected: Bool {
        // If monitor is nil because the class is not currently monitoring, then return false
        // Examine status property of currentPath for satisfied condition, then device is connected.
        guard let monitor = monitor else { return false }
        return monitor.currentPath.status == .satisfied
    }
    
    // Proceed only if the monitor property has an actual value. currentPath gives availableINterfaces with a collection of NWInterface objects. Filter returns
    // a collection which contains only the interface that is being used at the moment of the request.
    var interfaceType: NWInterface.InterfaceType? {
        guard let monitor = monitor else { return nil }
        
        return monitor.currentPath.availableInterfaces.filter {
            monitor.currentPath.usesInterfaceType($0.type)
        }.first?.type
    }
    
    var availableInterfacesTypes: [NWInterface.InterfaceType]? {
        // create a new array of interfacetype objects based on the original NWINterface collection and give back the available interface.
        guard let monitor = monitor else { return nil }
        return monitor.currentPath.availableInterfaces.map { $0.type }
    }
    
    var isExpensive: Bool {
        return monitor?.currentPath.isExpensive ?? false
    }
    
    func startMonitoring() {
        // Check if monitoring already
        guard !isMonitoring else { return }
        
        monitor = NWPathMonitor()
        // Running on background thread
        let queue = DispatchQueue(label: "NetStatus_Monitor")
        monitor?.start(queue: queue)
        
        // Notifying caller when network changes occur
        monitor?.pathUpdateHandler = { _ in
            self.netStatusChangeHandler?()
        }
        
        isMonitoring = true
        didStartMonitoringHandler?()
    }
    
    func stopMonitoring() {
        // check if currently monitoring
        guard isMonitoring, let monitor = monitor else { return }
        monitor.cancel()
        self.monitor = nil
        isMonitoring = false
        didStopMonitoringHandler?()
    }
}
