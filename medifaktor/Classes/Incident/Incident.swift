import Foundation

class Incident {
    var location: String
    var type: String
    var description: String
    var image: String
    var isAccepted: Bool
    
    init(location: String, type: String, description: String, image: String, isAccepted: Bool) {
        self.location = location
        self.type = type
        self.description = description
        self.image = image
        self.isAccepted = isAccepted
    }
    
    convenience init() {
        self.init(location: "", type: "", description: "", image: "", isAccepted: false)
    }
}
