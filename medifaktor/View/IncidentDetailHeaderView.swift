//
//  IncidentDetailHeaderView.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import UIKit
import MapKit

class IncidentDetailHeaderView: UIView {

    @IBOutlet var mapView: MKMapView!
    @IBOutlet var locationLabel: UILabel!
    
    @IBOutlet var typeLabel: UILabel! {
        didSet {
            typeLabel.layer.cornerRadius = 5.0
            typeLabel.layer.masksToBounds = true
        }
    }
    
    @IBOutlet var acceptedImageView: UIImageView! {
        didSet {
            acceptedImageView.image = UIImage(named: "heart-tick")?.withRenderingMode(.alwaysTemplate)
            acceptedImageView.tintColor = .darkGray
        }
    }
    
    func configure(location: String) {
        // Get Geolocation
        let geoCoder = CLGeocoder()
        
        print(location)
        geoCoder.geocodeAddressString(location, completionHandler: { placemarks, error in
            if let error = error {
                print(error.localizedDescription)
                return
            }
            
            if let placemarks = placemarks {
                // get first placemark
                let placemark = placemarks[0]
                
                // Add annotation
                let annotation = MKPointAnnotation()
                
                if let location = placemark.location {
                    // Display annotation
                    annotation.coordinate = location.coordinate
                    self.mapView.addAnnotation(annotation)
                    
                    // Set zoom
                    let region = MKCoordinateRegion(center: annotation.coordinate, latitudinalMeters: 250, longitudinalMeters: 250)
                    self.mapView.setRegion(region, animated: false)
                }
            }
        })
    }
}
