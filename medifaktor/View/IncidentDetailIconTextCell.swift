//
//  IncidentDetailIconTextCell.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import UIKit

class IncidentDetailIconTextCell: UITableViewCell {

    @IBOutlet var iconImageView: UIImageView!
    
    @IBOutlet var shortTextLabel: UILabel! {
        didSet{
            shortTextLabel.numberOfLines = 0
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
