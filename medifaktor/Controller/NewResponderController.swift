//
//  NewResponderController.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import UIKit

class NewResponderController: UITableViewController, UITextFieldDelegate {

    @IBOutlet var firstNameTextField: UITextField! {
        didSet {
            firstNameTextField.tag = 1
            firstNameTextField.becomeFirstResponder()
            firstNameTextField.delegate = self
        }
    }
    
    @IBOutlet var lastNameTextField: UITextField! {
        didSet {
            lastNameTextField.tag = 2
            lastNameTextField.delegate = self
        }
    }
    
    @IBOutlet var phoneNumberTextField: UITextField! {
        didSet {
            phoneNumberTextField.tag = 3
            phoneNumberTextField.delegate = self
        }
    }
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.tintColor = .white
        navigationController?.navigationBar.shadowImage = UIImage()
        if let customFont = UIFont(name: "Rubik-Medium", size: 25.0) {
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 231, green: 75, blue: 60, alpha: 1), NSAttributedString.Key.font: customFont]
        }
    }
    
    // MARK: - TextField Delegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let nextTextField = view.viewWithTag(textField.tag  + 1) {
            textField.resignFirstResponder()
            nextTextField.becomeFirstResponder()
        }
        
        return true
    }

}
