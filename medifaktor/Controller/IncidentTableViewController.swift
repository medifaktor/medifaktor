//
//  IncidentTableViewController.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import UIKit
import Alamofire

class IncidentTableViewController: UITableViewController {
    
    @IBAction func unwindToHome(segue: UIStoryboardSegue) {
        dismiss(animated: true, completion: nil)
    }
    
    var incidents:[Incident] = [
        Incident(location: "Fichtenstrasse 30, 82256 Fürstenfeldbruck", type: "CPR", description: "Test Description 1", image: "phone", isAccepted: false),
        Incident(location: "Leopoldstrasse 55, 80802 München", type: "CPR", description: "Test Description 2", image: "phone", isAccepted: false)
    ]
        
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // # MARK: NavigationController
        navigationController?.navigationBar.prefersLargeTitles = true
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.hidesBarsOnSwipe = false
        
        if let customFont = UIFont(name: "Rubik-Medium", size: 40.0) {
            navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 231.0/255.0, green: 76.0/255.0, blue: 60.0/255.0, alpha: 1.0), NSAttributedString.Key.font: customFont]
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        // Definition of cell
        let cellIdentifier = "dataCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! IncidentTableViewCell
        
        // Define Incident information for table view cell
        cell.locationLabel.text = incidents[indexPath.row].location
        cell.typeLabel.text = incidents[indexPath.row].type
        cell.thumbnailImageView.image = UIImage(named: incidents[indexPath.row].image)
        
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return incidents.count
    }
    
    // MARK: - Leading/Trailing TableView Actions
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        // Delete
        let deleteAction = UIContextualAction(style: .destructive, title: "Entfernen") { (action, sourceView, completionHandler) in
            
            self.incidents.remove(at: indexPath.row)

            self.tableView.deleteRows(at: [indexPath], with: .fade)
            completionHandler(true)
        }
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [deleteAction])
        
        return swipeConfiguration
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let acceptAction = UIContextualAction(style: .normal, title: "Akzeptieren") { (action, sourceView, completionHandler) in
            //let cell = tableView.cellForRow(at: indexPath) as! IncidentTableViewCell
            self.incidents[indexPath.row].isAccepted = (self.incidents[indexPath.row].isAccepted) ? false : true
            completionHandler(true)
        }
        
        let acceptIcon = incidents[indexPath.row].isAccepted ? "undo": "tick"
        acceptAction.image = UIImage(named: acceptIcon)
        
        let swipeConfiguration = UISwipeActionsConfiguration(actions: [acceptAction])
        
        return swipeConfiguration
    }
    
    
    // MARK: - Prepare Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        // Incident Detail View
        if segue.identifier == "showIncidentDetail" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationController = segue.destination as! IncidentDetailViewController
                destinationController.incident = incidents[indexPath.row]
            }
        }
    }

    

}
