//
//  IncidentDetailViewController.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import UIKit
import MapKit

class IncidentDetailViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet var incidentMapView: MKMapView!
    @IBOutlet var tableView: UITableView!
    @IBOutlet var headerView: IncidentDetailHeaderView!
    
    var incident = Incident()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // TableView Setup
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.tintColor = .white
        
        // Configure header view
        headerView.locationLabel.text = incident.location
        headerView.typeLabel.text = incident.type
        headerView.acceptedImageView.isHidden = (incident.isAccepted) ? false: true

        // MARK: - MapView
        // Set mapview
        headerView.configure(location: incident.location)

        tableView.contentInsetAdjustmentBehavior = .never
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - StatusBar
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    // MARK: - Datasource / Delegate
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.row {
        
        // Cell with incident location information
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: IncidentDetailIconTextCell.self), for: indexPath) as! IncidentDetailIconTextCell
            cell.iconImageView.image = UIImage(named: "phone")
            cell.shortTextLabel.text = incident.location
            cell.selectionStyle = .none
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: IncidentDetailTextCell.self), for: indexPath) as! IncidentDetailTextCell
            cell.descriptionLabel.text = incident.description
            cell.selectionStyle = .none
            
            return cell
            
        default:
            fatalError("Failed to instantiate the table view cell")
        }
    }
    
    // MARK: - Segues
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showMap" {
            let destinationController = segue.destination as! MapViewController
            destinationController.incident = incident
        }
    }
}
