//
//  Helper.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import Foundation
import UIKit

func adaptAlertControllerForIpad(tableView: UITableView, indexPath: IndexPath, alertController: UIAlertController) {
    if let popoverController = alertController.popoverPresentationController {
        if let cell = tableView.cellForRow(at: indexPath) {
            popoverController.sourceView = cell
            popoverController.sourceRect = cell.bounds
        }
    }
}
