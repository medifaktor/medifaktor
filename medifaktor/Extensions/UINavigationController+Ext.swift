//
//  UINavigationController+Ext.swift
//  medifaktor
//
//  Created by Sebastian Scharf on 17.08.19.
//  Copyright © 2019 Sebastian Scharf. All rights reserved.
//

import Foundation
import UIKit

extension UINavigationController {
    open override var childForStatusBarStyle: UIViewController? {
        return topViewController
    }
}
